#include <iostream>

/*
	Esta funcion dibuja un cuadrado de asteriscos con la longitud de lado especificada
	ejemplo lado = 3
	***
	***
	***
*/
void DrawSquare(const unsigned int& lado)
{
	for (int i = 0; i < lado; i++) {
		for (int j = 0; j < lado; j++) {
			std::cout << "*";
		}
		std::cout << std::endl;
	}
}

void DrawTree(const unsigned int& altura)
{
	int n = 2 * altura - 1;
	if (altura > 1 && altura < 25) {
		for (int i = 0; i < (altura + 3); ++i) {
			for (int j = 0; j <= n; ++j) {
				if (i >= altura && j==((n+1)/2)) {
					std::cout << "|";
				}else if (j >= (((n + 1) / 2) - i) && j <= (((n + 1) / 2) + i) && i < altura) {
					std::cout << "*";
				}
				else {
					std::cout << " ";
				}
			}
			std::cout << std::endl;
		}
	}
}


void main(void)
{
	int num;
	bool aprove = false;
	//DrawSquare(10);
	do {
		std::cin >> num;
		if (num >= 1 && num <= 24) {
			aprove = true;
		}
		else {
			std::cout<<"Entrada incorrecta, intente con otro numero"<<std::endl;
		}
	} while (!aprove);
	DrawTree(num);
}

/*

 Ejemplo para la tarea del arbol con altura = 5  (el pie siempre es de 3)
	*            nivel 1
   ***			 nivel 2
  *****			 nivel 3
 ******* 		 nivel 4
*********		 nivel 5
	|             Pie
	|             pie
	|             pie

*/