#pragma once

class Node
{
public:
	int value;
	Node* left;
	Node* right;
	Node(const int& value);

};

typedef Node* ptrNode;
