#pragma once
#include "Node.h"

class BinaryTree
{
private:
	Node* root;
	void insertRecursive(ptrNode& root, ptrNode& newNode);
	void printRecursiveInfix(ptrNode& root);
	void printReversed(ptrNode& root);

public:
	void insert(const int& value);
	void printInorder();  //primero izq, luego el de referencia, luego el derecho
	//void printPreorder(); //primero el de referencia, luego el izq, y luego el der
	//void printPostorder();//primero el de la izquierda, luego el de la derecha, luego el de de referencia	
	void printReversed();

	BinaryTree();
};
