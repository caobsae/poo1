#include "BinaryTree.h"
#include <iostream>

void BinaryTree::insertRecursive(ptrNode& root, ptrNode& newNode)
{
	if (root == nullptr)  //caso 1: no existe el nodo de referencia
		root = newNode;
	else if (newNode->value < root->value) //caso 2: se tiene que insertar por la izq.
		insertRecursive(root->left, newNode);
	else //caso 3: se tiene que insertar por la derecha por ser mayor o igual
		insertRecursive(root->right, newNode);
}

void BinaryTree::printRecursiveInfix(ptrNode& root)
{
	if (root == nullptr)
		return;

	printRecursiveInfix(root->left);
	std::cout << root->value << std::endl;
	printRecursiveInfix(root->right);
}

void BinaryTree::printReversed(ptrNode& root) {
	if (root->right != nullptr) {
		printReversed(root->right);
	}

	std::cout << root->value << std::endl;

	if (root->left != nullptr) {
		printReversed(root->left);
	}
}

void BinaryTree::insert(const int& value)
{
	Node* temp = new Node(value);
	insertRecursive(root, temp);
}

void BinaryTree::printInorder()
{
	printRecursiveInfix(root);
}

void BinaryTree::printReversed() {
	if (root != nullptr) {
		printReversed(root);
	}
}


//Impresión PreOrder Recursivo
void BinaryTree::printPreOrder() {
	if (root != nullptr) {
		printPreOrderRecursive(root);
	}
}

void BinaryTree::printPreOrderRecursive(ptrNode& root) {
	std::cout << root << ", ";
	if (root->right != nullptr) {
		printPreOrderRecursive(root->right);
	}
	if (root->left != nullptr) {
		printPreOrderRecursive(root->left);
	}
}

//Impresión PostOrder recursivo
void BinaryTree::printPostOrder() {
	if (root != nullptr) {
		printPostOrderRecursive(root);
	}
}

void BinaryTree::printPostOrderRecursive(ptrNode& root) {
	std::cout << root << ", ";
	if (root->left != nullptr) {
		printPreOrderRecursive(root->left);
	}
	if (root->right != nullptr) {
		printPreOrderRecursive(root->right);
	}
	std::cout << root << ", ";
}

BinaryTree::BinaryTree() : root(nullptr)
{}