#include <iostream>

using std::string;
string controllerCases(int counter) {
	switch (counter){
	case 1:
		return "Arriba";
		break;
	case 2:
		return "Derecha";
		break;
	case 3:
		return "Abajo";
		break;
	case 4:
		return "Izquierda";
		break;
	case 5:
		return "Triangulo";
		break;
	case 6:
		return "Circulo";
		break;
	case 7:
		return "Equis";
		break;
	case 8:
		return "Cuadrado";
		break;
	default:
		return "limite de bit excedido";
		break;
	}
}
void printControllerState(const unsigned char& value) {
	int bitCounter = 9;
	int bitValue = 128;
	int intValue = (int)value;
	//Imprimir los estados de los botones y la palanca
	for (int i = 0; i < 8; i++) {
		if (intValue >= bitValue) {
			std::cout << controllerCases(bitCounter-1) + " ";
			intValue -= bitValue;
		}
		bitCounter--;
		bitValue = bitValue / 2;
		if (intValue == 0) {
			break;
		}
	}
	//if (value & 1 == 1)
		//palanca arriba

	std::cout << std::endl;
}

int main() {
	for (int i = 0; i < 20; ++i) {
		unsigned char value = rand() % 255;
		std::cout << value << "  -> " <<(int)value<<" ->";
		printControllerState(value);
	}
}