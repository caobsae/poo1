#define _CRT_NONSTDC_NO_DEPRECATE
#include <iostream>
#include <string>

/*

base 2  = binario   (0, 1)  // 0, 1, 10, 11, 100                     11b
base 8  = octal   (0 - 7)  //0, 1, 2, 3, 4, 5, 6, 7, 10, 11...17, 20.. 77               77o
base 10
base 16  = hexadecimal  (0 - 9, A - F)   0...9, A, B, C, D, E, F, 10, 11, 12, 13-19, 1A, 1B...1F, 20... FF, 100    ejemplo: 0x1A

Idea o algoritmo:
    dividir num entre la base y continuar dividiendo los cocientes hasta que el cociente sea cero
    y despues se concatena el ultimo cociente con los residuos en orden inverso

Ejemplo 1:
num = 20  a base 16

                             __1____
  (div) num / 16 = 1     16 | 20             -> 0x14
  (mod) num % 16 = 4           4

Ejemplo 2:
num 300  a base 16
    1. 300 / 16 = 18
    2  300 % 16 = 12

    3. 18 / 16 = 1
    4. 18 % 16 = 2
    ultimo cociente + todos los residuos del ultimo hacia el primero   "1" + "2" + "12 -> c"    ->012C

ejemplo 3:
num 20  a base 2

    1. 20 / 2 = 10
    2. 20 % 2 = 0
    3. 10 / 2 = 5
    4. 10 % 2 = 0
    5. 5 / 2 = 2
    6. 5 % 2 = 1
    7. 2 / 2 = 1
    8. 2 % 2 = 0
    0001 0100

ejemplo 4
num 20 a base 8
    1. 20 / 8  = 2
    2. 20 & 8  = 4
    3. 2 / 8   = 0
    4. 2 & 8   = 2

    024o
    1 2 3 4 5 6 7 8  9  10 11 12 13 14 15 16 17 18 19 20   base 10
    1 2 3 4 5 6 7 10 11 12 13 14 15 16 17 20 21 22 23 24   base 8

-------------------------------------------------------------
Como convertir un numero a string ??
    std::to_string (25) ->"25"   <- Prefieran usar este metodo !!!!
    std::itoa(25)   -> "25"

*/



std::string BaseConverter(const unsigned int& num, const unsigned int& base)
{
    //tip: para representar un numero > 9 como un solo digito, se usaran letras y se pueden calcular mas o menos asi:
    //if (value > 9)   
    //   texto = texto + std:itoa('A' + (value - 10))
    char buff[64];
    itoa(num, buff, base);
    std::string text = buff;
    return text;
}


/*
Ejemplos para capturar datos desde el teclado
    int num  = 0;
    std::cin >> num;
    std::cout << num;

    std::string  s;
    std::cin >> s;
    std::cout << s;
*/

void main(void)
{
    //mientras la base sea > 1 y < 37, repetir
    int num, base;
    do
    {
        std::cin >> num;
        std::cin >> base;
        if (base > 1 && base < 37)
            std::cout << BaseConverter(num, base) << std::endl;
    } while (base > 1 && base < 37);
}